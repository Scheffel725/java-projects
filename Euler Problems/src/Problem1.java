import java.util.*;

public class Problem1 {
	
	static int sum = 0;
	
	static public void main(String[] arguments) {
		for (int i = 0; i < 1000; i++) {
			if (i % 3 == 0 || i % 5 == 0)
				sum += i;
		}
		
		System.out.println(sum);
	}
}