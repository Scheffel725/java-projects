import java.util.*;

public class Problem2 {
	
	final static int MAX = 4000000;
	static int sum = 0;
	
	public static void main(String[] arguments) {
		for (int i = 0; i <= 100; i++) {
			if (fibonacci(i) < MAX) {
				int n = fibonacci(i);
				if (n % 2 == 0)
					sum = sum + n;
			}
		}
		
		System.out.println(sum);
	}
	
	public static int fibonacci(int n) {
		if (n == 0)
			return 0;
		else if (n == 1)
			return 1;
		else
			return fibonacci(n - 1) + fibonacci(n - 2);
	}
}