import java.util.*;

public class Problem3 {	
	static int factor = 0;
	
	public static void main(String[] arguemtns) {
		Scanner console = new Scanner(System.in);
		System.out.println("What number would you like to factor? ");
		
		long input = console.nextLong();
		
		for (int i = 2; i <= input; i++) {
			if (input % i == 0) {
				factor = i;
				input /= i;
			}
		}
		
		System.out.println(factor);
	}
}